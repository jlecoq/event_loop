package com.eventloop.ThreadPauser

class ThreadPauser {
    private var isPaused = false
    private val mutex = Object()
    
    fun isPaused(): Boolean {
        return isPaused
    }

    fun pause() {
        synchronized(mutex) {
            isPaused = true
            mutex.wait()
        }
    }

    fun resume() {
        synchronized(mutex) {
            isPaused = false
            mutex.notify()
        }
    }
}
