package com.eventloop.tests

import com.eventloop.setTimeout.setTimeout
import com.eventloop.setInterval.setInterval
import com.eventloop.Bluetooth.Bluetooth
import com.eventloop.IntensiveWork.IntensiveWork
import com.eventloop.intensiveWork.intensiveWork

fun testSetInterval() {
    var number_of_calls = 0

    setInterval(100) {
        println("Hello world 1, thread id: ${Thread.currentThread().id}")

        number_of_calls++
        if (number_of_calls == 10) {
            it()
        }
    }

    var number_of_calls2 = 0

    setInterval(100) {
        println("Hello world 2, thread id: ${Thread.currentThread().id}")

        number_of_calls2++ 
        if (number_of_calls2 == 10) {
            it()
        }
    }
}

fun testSetTimeout() { 
    setTimeout(1500) {
        println("Hello world 1, thread id: ${Thread.currentThread().id}")
    }

    setTimeout(1500) {
        println("Hello world 2, thread id: ${Thread.currentThread().id}")
    }
}

fun testIntensiveWorkFunc() {
    for (i in 0..100) {
        intensiveWork() { data ->
            println("Data: $data, thread id: ${Thread.currentThread().id}")
        }
    }
}

fun testIntensiveWorkClass() {
    for (i in 0..100) {
        val worker = IntensiveWork()

        worker.onFinish { data ->
            println("Data: $data, thread id: ${Thread.currentThread().id}")
        }

        worker.start()
    }
}

fun testBluetooth() {
    val bluetooth = Bluetooth()

    val macAdd = "7B:5F:21:DA:D1:B2" 
    val UUID = "14234aqd-a26d-zq7d-9dqb-fba3qdjiqz32e"
    val socket = bluetooth.connect(macAdd, UUID)
    
    socket.readInfinitely { data ->
        println("Data: $data, thread id: ${Thread.currentThread().id}")
    }

    socket.onDisconnect { message ->
        println("$message, thread id: ${Thread.currentThread().id}")
    }
}
