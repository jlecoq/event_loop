package com.eventloop.setInterval

import com.eventloop.types.*
import java.util.Timer
import kotlin.concurrent.timerTask
import com.eventloop.EventLoop.EventLoop

fun setInterval(millis: Long = 1000, exec: ParamCallback<Callback>): Callback {
    val timer = Timer()
    val cancellable = {
        EventLoop.decreaseAsyncCall()
        timer.cancel()
    }

    EventLoop.increaseAsyncCall()
    timer.scheduleAtFixedRate(timerTask {
        EventLoop.enqueue { exec(cancellable) }
    }, 0, millis)

    return cancellable 
}
