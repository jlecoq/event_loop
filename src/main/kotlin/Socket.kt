package com.eventloop.Socket

import kotlin.concurrent.thread
import kotlin.random.Random
import com.eventloop.types.ParamCallback
import com.eventloop.EventLoop.EventLoop
import com.eventloop.setInterval.setInterval

class Socket {
    var onDisconnectCb: ParamCallback<String> = { }

    fun readInfinitely(onData: ParamCallback<String>) { 
        var number_of_read = 0

        setInterval(500) { cancel ->
            number_of_read++

            val randomNumber = Random.nextInt(0, 100).toString()
            EventLoop.enqueue {
                onData(randomNumber)
            }

            if (number_of_read == 8) {
                EventLoop.enqueue {
                    onDisconnectCb("The connection has been closed by the remote device")
                }
                cancel()
            }
        }
    }
    
    fun onDisconnect(cb: ParamCallback<String>) {
        onDisconnectCb = cb
    }
}
