package com.eventloop.IntensiveWork

import com.eventloop.EventLoop.EventLoop
import com.eventloop.types.ParamCallback
import com.eventloop.Worker.Worker

class IntensiveWork: Worker<Int>() {
    override fun job(): Int {
        var a = 0

        for (i in 0..1_000_000_000) {
            a++
        } 

        return a
    }
}

