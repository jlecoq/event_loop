package com.eventloop.setTimeout

import com.eventloop.types.*
import java.util.Timer
import kotlin.concurrent.thread
import com.eventloop.EventLoop.EventLoop

fun setTimeout(millis: Long = 1000, exec: Callback) {
    EventLoop.increaseAsyncCall()
    thread {
        Thread.sleep(millis)
        EventLoop.enqueue {
            exec()
            EventLoop.decreaseAsyncCall()
        }
    }
}
