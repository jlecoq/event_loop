package com.eventloop.types

typealias Callback = () -> Unit
typealias ParamCallback<T> = (T) -> Unit
typealias ReturnCallback<T> = () -> T
