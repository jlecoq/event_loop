package com.eventloop.Worker

import com.eventloop.EventLoop.EventLoop
import com.eventloop.types.ParamCallback
import com.eventloop.types.Callback
import kotlin.concurrent.thread

abstract class Worker<T> {
    private var onFinishCb: ParamCallback<T> = {}
    
    init {
        EventLoop.increaseAsyncCall()
    }

    fun onFinish(cb: ParamCallback<T>) {
        onFinishCb = cb
    }
    
    fun start() {
        thread {
            val result = job()
            EventLoop.enqueue { 
                onFinishCb(result)
                EventLoop.decreaseAsyncCall()
            }
        }
    }

    abstract fun job(): T
}

