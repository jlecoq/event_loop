import com.eventloop.EventLoop.EventLoop
import com.eventloop.tests.* 

fun mainCode() {
    testSetTimeout()
    testIntensiveWorkClass()
    testIntensiveWorkFunc()
    testSetInterval()
    testBluetooth()
}

fun main(args: Array<String>) {
    EventLoop.start(::mainCode)
}
