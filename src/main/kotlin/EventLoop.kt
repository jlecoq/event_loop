package com.eventloop.EventLoop

import com.eventloop.types.Callback
import com.eventloop.ThreadPauser.ThreadPauser
import java.util.concurrent.ConcurrentLinkedQueue
import java.util.concurrent.atomic.AtomicInteger

object EventLoop {
    private val queue = ConcurrentLinkedQueue<Callback>()
    private val threadPauser = ThreadPauser()
    private var asyncCall = AtomicInteger(0)

    fun start(init: Callback) {
        init()

        while (queue.isNotEmpty() || asyncCall.get() != 0) {
            queue.poll()?.invoke()

            if (queue.isEmpty() && asyncCall.get() == 0) {
                return
            }
            
            if (queue.isEmpty()) {
                threadPauser.pause()
            }
        }
    }

    fun enqueue(callback: Callback) {          
        queue.add(callback) 
        if (threadPauser.isPaused()) {
            threadPauser.resume()
        }
    }

    fun increaseAsyncCall() {
        asyncCall.getAndIncrement()
    }

    fun decreaseAsyncCall() {
        asyncCall.getAndDecrement()
    }
}
