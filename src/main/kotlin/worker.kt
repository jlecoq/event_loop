package com.eventloop.worker

import kotlin.concurrent.thread
import com.eventloop.types.Callback
import com.eventloop.types.ParamCallback
import com.eventloop.types.ReturnCallback
import com.eventloop.EventLoop.EventLoop

fun <T> worker(onFinish: ParamCallback<T>, job: ReturnCallback<T>) {
    thread {
        EventLoop.increaseAsyncCall()
        val result = job()
        EventLoop.enqueue { 
            onFinish(result) 
            EventLoop.decreaseAsyncCall()
        }
    }
}
