package com.eventloop.intensiveWork

import com.eventloop.EventLoop.EventLoop
import com.eventloop.types.ParamCallback
import com.eventloop.worker.worker

fun intensiveWork(onFinish: ParamCallback<Int>) {
    worker(onFinish) { 
        var a = 0

        for (i in 0..1_000_000_000) {
            a++
        } 

        a
    }
}
